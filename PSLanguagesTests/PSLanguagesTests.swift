//
//  PSLanguagesTests.swift
//  PSLanguagesTests
//
//  Created by Vasil Nunev on 12/05/2017.
//  Copyright © 2017 nunev. All rights reserved.
//

import XCTest
@testable import PSLanguages

class PSLanguagesTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let storage = FileFetcher()
        do {
            try Localisation.loadLocalisationStrings(with: storage.fetchLocalizationData(withFailure: false)!)
        }catch let error {
            XCTAssert(false, "wrong JSON format")
        }
    }
    
    func testCloseTextFinnish() {
        Localisation.locale = "fi"
        let exResult = "Sulje"
        let result = L("Close")
        XCTAssertEqual(exResult, result)
    }
    
    func testCloseTextGerman() {
        Localisation.locale = "de"
        let exResult = "Close" // This passes because no translation is available for german right now
        let result = L("Close")
        XCTAssertEqual(exResult, result)
    }

    func testCloseTextFrench() {
        Localisation.locale = "fr"
        let exResult = "Fermer"
        let result = L("Close")
        XCTAssertEqual(exResult, result)
    }
    
    func testCloseTextPortuguese() {
        Localisation.locale = "pt"
        let exResult = " Fechar" //TODO: This shouldn't be the case as in the json it comes with a space at the beggining, it shouldn't!
        let result = L("Close")
        XCTAssertEqual(exResult, result)
    }
    
    func testCloseTextEnglishGB() {
        Localisation.locale = "en-gb"
        let exResult = "Close"
        let result = L("Close")
        XCTAssertEqual(exResult, result)
    }
    
    
    func testForgottenPassTextFinnish() {
        Localisation.locale = "fi"
        let exResult = "Oletko unohtanut salasanasi? Tilaa uusi salasana sähköpostiisi [tästä]."
        let result = L("Forgotten your password? Click [here].")
        XCTAssertEqual(exResult, result)
    }
    
    func testForgottenPassTextFrench() {
        Localisation.locale = "fr"
        let exResult = " Mot de passe oublié? Cliquez [ici]."//TODO: Again space at the start of the text
        let result = L("Forgotten your password? Click [here].")
        XCTAssertEqual(exResult, result)
    }
    
    func testForgottenPassTextPortuguese() {
        Localisation.locale = "pt"
        let exResult = "Esqueceu sua senha? Clique [aqui]."
        let result = L("Forgotten your password? Click [here].")
        XCTAssertEqual(exResult, result)
    }
    
    func testForgottenPassTextSpanish() {
        Localisation.locale = "es"
        let exResult = "Forgotten your password? Click [here]." //No translation for spanish
        let result = L("Forgotten your password? Click [here].")
        XCTAssertEqual(exResult, result)
    }
    
    func testForgottenPassTextBritish() {
        Localisation.locale = "en-gb"
        let exResult = "Forgotten your password? Click [here]."
        let result = L("Forgotten your password? Click [here].")
        XCTAssertEqual(exResult, result)
    }
    
    func testLogOutTextFinnish() {
        Localisation.locale = "fi"
        let exResult = "Kirjaudu ulos"
        let result = L("Log out")
        XCTAssertEqual(exResult, result)
    }
    
    func testLogOutTextFrench() {
        Localisation.locale = "fr"
        let exResult = " Connectez - Out" //TODO: Another space at the beggining of the text
        let result = L("Log out")
        XCTAssertEqual(exResult, result)
    }
    
    func testLogOutTextPortuguese() {
        Localisation.locale = "pt"
        let exResult = "Sair"
        let result = L("Log out")
        XCTAssertEqual(exResult, result)
    }
    
    func testLogOutTextEnglish() {
        Localisation.locale = "en"
        let exResult = "Log out"
        let result = L("Log out")
        XCTAssertEqual(exResult, result)
    }
    
    func testLogOutTextBritish() {
        Localisation.locale = "en-gb"
        let exResult = "Log out"
        let result = L("Log out")
        XCTAssertEqual(exResult, result)
    }
    
    func testLoadWrongJSONWithDoTry() {
        let storage = FileFetcher()
        do {
            try Localisation.loadLocalisationStrings(with: storage.fetchLocalizationData(withFailure: true)!)
        }catch let error {
            XCTAssert(true, error.localizedDescription)
        }
    }
    
    func testLoadWrongJSONWithTry() {
        let storage = FileFetcher()
        try? Localisation.loadLocalisationStrings(with: storage.fetchLocalizationData(withFailure: true)!)
        Localisation.locale = "pt"
        let exResult = "Log out"
        let result = L("Log out")
        XCTAssertEqual(exResult, result)
    }
 }
