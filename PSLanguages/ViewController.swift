//
//  ViewController.swift
//  PSLanguages
//
//  Created by Vasil Nunev on 12/05/2017.
//  Copyright © 2017 nunev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let storage = FileFetcher()
        try? Localisation.loadLocalisationStrings(with: storage.fetchLocalizationData(withFailure: false)!)
        print(L("Log out"))
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

