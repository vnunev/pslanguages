//
//  PSFileSystemController.swift
//  PSReader
//
//  Created by Mark Godden on 02/05/2016.
//  Copyright © 2016 PageSuite. All rights reserved.
//

import UIKit

class FileSystemController {
    static let shared = FileSystemController()
    
    func returnDataForFileName(_ filePath: String) -> Data? {
        let docPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory,FileManager.SearchPathDomainMask.userDomainMask, true)
        
        var url: String = ""
        
        if let documentDirectory = docPaths.first {
            url = documentDirectory + "/zip/\(filePath)"
        }
        
        if let data: Data = try? Data(contentsOf: URL(fileURLWithPath: url)) {
            return data
        }
        
        return nil
    }
    
    func returnDataFromLocalJSON(file name: String) -> Data? {
        if let path = Bundle.main.url(forResource: name, withExtension: "json"), let dataFromPath = try? Data(contentsOf: path) {
            return dataFromPath
        }
        return nil
    }
}
