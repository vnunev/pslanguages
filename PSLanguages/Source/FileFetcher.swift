//
//  FileFetcher.swift
//  PSLanguages
//
//  Created by Vasil Nunev on 12/05/2017.
//  Copyright © 2017 nunev. All rights reserved.
//

import Foundation


class FileFetcher: StorageLayerProtocol {
    private var url = { () -> URL in
        let docPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory,FileManager.SearchPathDomainMask.userDomainMask, true)
        
        var url = ""
        
        if let documentDirectory = docPaths.first {
            url = documentDirectory + "/zip/localized.json"
        }
        return URL(fileURLWithPath:url)
    }()
    
    func fetchLocalizationData(withFailure fail: Bool) -> Data? {
        if fail {
            if let dataPath = Bundle(for: FileFetcher.self).url(forResource: "localizedFail", withExtension: "json"), let data = try? Data(contentsOf: dataPath) {
                return data
            }
        }else {
            if let data = try? Data(contentsOf: self.url) {
                return data
            }else if let dataPath = Bundle(for: FileFetcher.self).url(forResource: "localized", withExtension: "json"), let data = try? Data(contentsOf: dataPath) {
                return data
            }
        }
        return nil
    }
}
