//
//  Localisation.swift
//  PSLive
//
//  Created by James Wilkinson on 16/08/2016.
//  Copyright © 2016 PageSuite. All rights reserved.
//

import Foundation

///Convenient wrapper function to work with the Localisation class.
public func L(_ term: String) -> String {
    return Localisation.localise(text: term)
}

/**
 Manages loaded localisation strings and localises given text.
 
 Steps to use:
 1. Load a language code using `loadLocalisationStrings()`
 2. Localise your text using `localise(_:)` or the more extensible `localise(_:withModificationBlock:)`
 3. There is no step 3
 */
@objc
public class Localisation : NSObject {
    
    enum Error: Swift.Error {
        case mistypedJSONError
    }
    
    static var locale: String = "en"    
    // Currently loaded localisation strings. Prevents needing to load from file every time
    internal static var storedLocalisation: NSDictionary? = nil
    
    //MARK: LOCALE FUNCTIONS
    
    /**
     Entry point for deciding which localisation to look for in the localisation dict.
     */
    private class func selectLocale(with data: Data) {
        //Grab the available languages from the localization.json file, plus the user's preferred languages.
        let availableLanguages: [String] = self.fetchAvailableLanguages(with: data)
        let preferredLanguages = NSLocale.preferredLanguages
        
        //Search for any direct matches. e.g. en-gb --- en-gb or fi --- fi
        let directMatches = filterLanguagesForList(availableLanguages: availableLanguages, preferredLanguages: preferredLanguages, clipped: false)
        
        if directMatches.count <= 0 {
            //No direct matches found. Split them to find any matches, e.g. en-gb --- en-us
            let directMatchesForClipped = filterLanguagesForList(availableLanguages: availableLanguages, preferredLanguages: preferredLanguages, clipped: true)
            
            if directMatchesForClipped.count > 0 {
                Localisation.locale = directMatchesForClipped[0]
                return
            }
        }
        
        //Either there are some direct matches, or nothing worked. In which case pass in all the languages and if necessary fall back to the development language (en-GB).
        if let bestMatchedLanguage = Bundle.preferredLocalizations(from: availableLanguages, forPreferences: nil).first {
            Localisation.locale = bestMatchedLanguage
        }
    }
    
    /**
     Takes a list of available languages from the server, along with a list of preferred languages (probably from the device).
     - returns: List of appropriate locales.
     - parameter availableLanguages - list of available localisations from the server.
     - parameter preferredLanguages - list of languages the user would like to use (based on device settings).
     - parameter clipped - e.g. if "en_gb" can't be found, look for "en".
     */
    private class func filterLanguagesForList(availableLanguages: [String], preferredLanguages: [String], clipped: Bool) -> [String] {
        //Order the available languages so they match the preferred languages.
        let sortedAvailables = preferredLanguages.flatMap { (prefLanguage) -> String? in
            for availableLanguage in availableLanguages {
                let splitAvail = availableLanguage.components(separatedBy: "-")[0]
                let splitPref = prefLanguage.components(separatedBy: "-")[0]
                
                if splitAvail == splitPref {
                    return availableLanguage
                }
            }
            
            return nil
        }
        
        let directMatches = sortedAvailables.filter { (availableLanguage) -> Bool in
            let availableLanguagePrefix = availableLanguage.components(separatedBy: "-")[0]
            
            for prefLanguage in preferredLanguages {
                if clipped {
                    let prefLanguageFix = prefLanguage.components(separatedBy: "-")[0]
                    if prefLanguageFix == availableLanguagePrefix {
                        return true
                    }
                } else {
                    if availableLanguage == prefLanguage {
                        return true
                    }
                }
            }
            
            return false
        }
        
        return directMatches
    }
    
    /**
     Parses the JSON localisations and looks for potential locales to later match against the user's device language.
     - returns: list of available languages from the languages JSON.
     */
    private class func fetchAvailableLanguages(with data: Data) -> [String] {
        var possibleLanguages: [String] = []
        var completeLanguages: [String] = []
        
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? NSDictionary, let translations = json["translations"] as? NSDictionary {
                for key in translations.allKeys {
                    
                    let dict: NSDictionary = translations[key] as! NSDictionary
                    for locale in dict.allKeys {
                        if locale as! String == "category" {
                            continue
                        }
                        
                        if !possibleLanguages.contains(locale as! String) {
                            possibleLanguages.append(locale as! String)
                        }
                    }
                }
                
                for possibleLanguage in possibleLanguages {
                    var complete = false
                    for key in translations.allKeys {
                        let dict: NSDictionary = translations[key] as! NSDictionary
                        for locale in dict.allKeys {
                            if (dict[locale] as! String) != "" && (locale as! String) == possibleLanguage {
                                complete = true
                            }
                        }
                    }
                    
                    if complete {
                        completeLanguages.append(possibleLanguage)
                    }
                }
            }
        } catch {
            
        }
        
        return completeLanguages
    }
    
    //MARK: DISK FUNCTIONS
    
    /**
     Load a localisation strings for a given language code. Loads from URL if not cached.
     - parameter completion: Completion that runs once the JSON is loaded from disk.
    */
    public class func loadLocalisationStrings(with data: Data) throws {
        do {
            let strings = try loadLocalisationJSON(with: data)
            self.selectLocale(with: data)
            self.storedLocalisation = strings
        }catch{
            self.storedLocalisation = nil
            throw error
        }
    }
    
    /**
     Loads localisation packaged with the app (typically en_GB)
     - returns: dictionary of localisations
     */
    private class func loadLocalisationJSON(with data: Data) throws -> NSDictionary {
        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary {
            return json
        }
        throw Error.mistypedJSONError
    }
    
    //MARK: TRANSLATIONS
    
    /**
     Localises text for given language code.
     This used to be a lot more complicated and flexible - i.e. you used to be able to modify translated text using a *modification handler*. Commit e61c86d was the last commit to have this functionality.
     However, this has to date not been used, so in the interest of making this easy to pick up (effectively now just a dictionary lookup), it has been significantly simplified.
     
     - returns: A localised string if found. Otherwise throws LocalisationError.LocalisedStringNotFound
     - parameter text: The text to be localised
     - parameter modify: If a localised string is found, it and the parameter are passed to this block allowing last minute insertion of values.
     If localised string not found, block is not called
     */
    @objc
    public class func localise(text: String) -> String {
        guard let loadedLocalisation = storedLocalisation else {
            return text
        }
        
        guard let localised = loadedLocalisation["translations"] as? NSDictionary else {
            return text
        }
        
        if  let localisedDict = localised[text] as? NSDictionary,
            let translation = localisedDict[Localisation.locale] as? String,
            translation != ""
        {
            return translation
        }
        
        return text
    }
    
    /// No need to init / subclass
    private override init() {super.init()}
}
