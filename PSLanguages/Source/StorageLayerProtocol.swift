//
//  StorageLayerProtocol.swift
//  PSLanguages
//
//  Created by Vasil Nunev on 12/05/2017.
//  Copyright © 2017 nunev. All rights reserved.
//

import Foundation

internal protocol StorageLayerProtocol: class {
    func fetchLocalizationData(withFailure fail: Bool) -> Data?
 }
